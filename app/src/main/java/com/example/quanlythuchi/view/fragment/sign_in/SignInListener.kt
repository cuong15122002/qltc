package com.example.quanlythuchi.view.fragment.sign_in

interface SignInListener {
    fun openLoginPhone()
    fun openSignInGoogle()
    fun openSignInFacebook()
    fun openApp()
}