package com.example.quanlythuchi.view.fragment.home.expense

interface ExpenseListener {
    fun openDayPicker()

    fun submitExpense()
}