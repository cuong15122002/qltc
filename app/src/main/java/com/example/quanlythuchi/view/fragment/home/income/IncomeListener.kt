package com.example.quanlythuchi.view.fragment.home.income

interface IncomeListener {
    fun openDayPicker()
    fun submitIncome()
}