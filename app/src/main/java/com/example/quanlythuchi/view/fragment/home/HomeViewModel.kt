package com.example.quanlythuchi.view.fragment.home

import com.example.quanlythuchi.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
@HiltViewModel
class HomeViewModel @Inject constructor() : BaseViewModel() {
}