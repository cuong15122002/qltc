package com.example.quanlythuchi.view.fragment.login_phone

interface LoginPhoneListener {
    fun loginToHome()
    fun backSignIn()
}