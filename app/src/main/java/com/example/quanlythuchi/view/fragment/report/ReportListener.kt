package com.example.quanlythuchi.view.fragment.report

interface ReportListener {
    fun openDayPicker()
    fun btnBackDay()
    fun btnNextDay()
}