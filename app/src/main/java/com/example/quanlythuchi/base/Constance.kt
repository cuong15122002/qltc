package com.example.quanlythuchi.base

object Constance {
    const val PHONE = "PHONE"
    const val OTP = "OTP"
    const val NAME_DATABASE = "QLTC"
    const val CATEGORY_EXPENSE = 0
    const val CATEGORY_INCOME = 1
    const val DATE_FORMAT = "dd/MM/yyyy"
    const val EXPENSE = "Expense"
    const val INCOME = "InCome"
    const val VND =" đ"
}